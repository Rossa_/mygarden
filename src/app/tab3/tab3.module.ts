/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { Tab3PageRoutingModule } from './tab3-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';

import { Tab3Component } from './tab3.page';

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    Tab3PageRoutingModule,
    RouterModule.forChild([{ path: '', component: Tab3Component }])
  ],
  declarations: [Tab3Component]
})
export class Tab3PageModule {}

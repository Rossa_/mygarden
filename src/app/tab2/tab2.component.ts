/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import {
  ActionSheetController,
  AlertController,
  ModalController
} from '@ionic/angular';
import { ToastNotificationService } from '../services/toast-notification.service';
import { GardenEditComponent } from './garden-edit/garden-edit.component';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { StorageService } from '../services/storage.service';
import { GardenSelected } from '../shared/garden.model';
import { TranslateService } from '@ngx-translate/core';
import { TourService } from '../services/tour.service';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.component.html',
  styleUrls: ['tab2.component.scss']
})
export class Tab2Component implements OnInit, OnDestroy {
  gardenSelected: GardenSelected;
  gardenSelectedSubscribe;
  isLoading = true;
  isDarkModeEnable = window.matchMedia('(prefers-color-scheme: dark)').matches;

  constructor(
    public modalController: ModalController,
    private storageService: StorageService,
    private alertController: AlertController,
    private translate: TranslateService,
    private tourService: TourService,
    private actionSheetCtrl: ActionSheetController,
    private toastNotificationService: ToastNotificationService
  ) {}

  async ngOnInit() {
    this.gardenSelectedSubscribe = this.storageService.gardenSelected.subscribe(
      (gardenSelect: GardenSelected) => {
        this.gardenSelected = gardenSelect;
        if (gardenSelect) {
          console.log(
            'Garden selected : ' +
              gardenSelect.garden.name +
              ' (Subscription) : ',
            gardenSelect
          );
        } else {
          console.log('Garden selected is null');
        }
        this.isLoading = false;
      }
    );
  }

  async onGardenActions() {
    const actionSheet = await this.actionSheetCtrl.create({
      header: this.translate.instant('TAB2.chooseAction'),
      buttons: [
        {
          text: this.translate.instant('TAB2.addGarden'),
          role: 'destructive',
          icon: 'add',
          handler: () => {
            this.addGarden();
          }
        },
        {
          text: this.translate.instant('TAB2.editGarden'),
          icon: 'create-outline',
          handler: () => {
            this.editGarden();
          }
        },
        {
          text: this.translate.instant('TAB2.deleteGarden'),
          role: 'destructive',
          icon: 'trash-outline',
          handler: () => {
            this.deleteGarden();
          }
        }
      ]
    });

    await actionSheet.present();
  }

  async addGarden() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('gardenTemplate');
    }
    const modal = await this.modalController.create({
      component: GardenEditComponent,
      componentProps: {
        editMode: false
      }
    });
    return await modal.present();
  }

  async editGarden() {
    const modal = await this.modalController.create({
      component: GardenEditComponent,
      componentProps: {
        editMode: true,
        gardenID: this.gardenSelected.garden.name
      }
    });
    return await modal.present();
  }

  async deleteGarden() {
    const alert = await this.alertController.create({
      header: this.translate.instant('TAB2.deleteGarden'),
      message: this.translate.instant('TAB2.deleteGardenConfirmation'),
      buttons: [
        {
          text: this.translate.instant('TAB2.canvas.no'),
          role: 'cancel'
        },
        {
          text: this.translate.instant('TAB2.canvas.yes'),
          role: 'confirm',
          handler: () => {
            this.storageService
              .deleteGarden(this.gardenSelected.garden.name)
              .then(async () => {
                await this.toastNotificationService.presentToast({
                  text: this.translate.instant('GLOBAL.gardenDeleted')
                });
              });
          }
        }
      ]
    });
    await alert.present();
  }

  async resetStorage() {
    await this.storageService.deleteGardens();
  }

  ngOnDestroy() {
    this.gardenSelectedSubscribe.unsubscribe();
  }

  protected readonly environment = environment;
}

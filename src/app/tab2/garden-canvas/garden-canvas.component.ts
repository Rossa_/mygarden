/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { ScreenOrientation } from '@capacitor/screen-orientation';
import { AfterViewInit, Component, OnDestroy } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { StorageService } from '../../services/storage.service';
import { GardenSelected } from '../../shared/garden.model';
import { TourService } from '../../services/tour.service';
import { EvolutionResponse } from './evolution.service';
import { TranslateService } from '@ngx-translate/core';
import { FabricService } from './fabric.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-canvas',
  templateUrl: 'garden-canvas.component.html',
  styleUrls: ['garden-canvas.component.scss']
})
export class GardenCanvasComponent implements AfterViewInit, OnDestroy {
  fabric: any;
  isLocked = true;
  currentYear = new Date().getFullYear();
  gardenSubscription: Subscription;
  gardenSelected: GardenSelected;

  constructor(
    private fabricService: FabricService,
    public alertController: AlertController,
    private storageService: StorageService,
    private translate: TranslateService,
    private tourService: TourService,
    private loadingCtrl: LoadingController
  ) {}

  ngAfterViewInit() {
    const internal = setInterval(() => {
      if (document.getElementById('canvasContainer').offsetHeight > 0) {
        clearInterval(internal);
        this.fabric = this.fabricService.initFabric(document);
        this.gardenSubscription = this.storageService.gardenSelected.subscribe(
          async (gardenSelect: GardenSelected) => {
            if (gardenSelect) {
              if (
                this.gardenSelected &&
                this.gardenSelected.date !== gardenSelect.date &&
                !this.isLocked
              ) {
                this.saveChangesConfirm().then(res => {
                  this.isLocked = true;
                  if (res === 'yes') {
                    this.fabricService.saveGardenDraw(
                      this.gardenSelected.garden.name,
                      this.gardenSelected.date.getFullYear(),
                      this.gardenSelected.date.getMonth()
                    );
                  }
                });
              }
              this.gardenSelected = gardenSelect;
              this.fabricService.restoreFromJSON(
                gardenSelect.garden.name,
                gardenSelect.date.getFullYear(),
                gardenSelect.date.getMonth()
              );
            }
          }
        );
      }
    }, 50);

    // TODO: Need to be fix
    ScreenOrientation.addListener('screenOrientationChange', () => {
      alert('Orientation Changed');
      setTimeout(() => {
        this.fabric = this.fabricService.initFabric(document);
      }, 100);
    });
  }

  async onEdit() {
    this.isLocked = !this.isLocked;
    if (this.isLocked) {
      this.fabricService.lockEdit();
      await this.fabricService.saveGardenDraw(
        this.gardenSelected.garden.name,
        this.gardenSelected.date.getFullYear(),
        this.gardenSelected.date.getMonth()
      );
    } else {
      if (this.tourService.isActive()) {
        this.tourService.goTo('addShapes');
      }
      this.fabricService.unlockEdit();
    }
  }

  async onAssignVegetables() {
    const alert = await this.alertController.create({
      header: this.translate.instant('TAB2.canvas.confirmation'),
      message: this.translate.instant('TAB2.canvas.gardenConfirmation'),
      buttons: [
        this.translate.instant('TAB2.canvas.no'),
        {
          text: this.translate.instant('TAB2.canvas.yes'),
          handler: () => {
            this.fabricService.lockEdit();
            this.fabricService
              .saveGardenDraw(
                this.gardenSelected.garden.name,
                this.gardenSelected.date.getFullYear(),
                this.gardenSelected.date.getMonth()
              )
              .then(async () => {
                const loading = await this.loadingCtrl.create({
                  message: this.translate.instant(
                    'TAB2.canvas.gardenGeneration'
                  )
                });
                await loading.present();
                this.fabricService
                  .vegetableAssignment(this.gardenSelected.garden.name)
                  .then(
                    async res => {
                      await loading.dismiss();
                      if (res.type !== 'success') {
                        const alert = await this.alertController.create({
                          header: this.translate.instant(
                            'GLOBAL.errors.somethingWrong'
                          ),
                          message: res.message,
                          buttons: ['OK']
                        });
                        await alert.present();
                      }
                    },
                    async (error: EvolutionResponse) => {
                      // An error occurred
                      await loading.dismiss();
                      const alert = await this.alertController.create({
                        header: this.translate.instant(
                          'GLOBAL.errors.somethingWrong'
                        ),
                        message: error.message,
                        buttons: ['OK']
                      });
                      await alert.present();
                    }
                  );
                this.isLocked = true;
              });
          }
        }
      ]
    });

    await alert.present();
  }

  onAddRect() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('drawingGuideLine');
    }
    this.fabricService.addRect();
  }

  onAddCircle() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('drawingGuideLine');
    }
    this.fabricService.addCircle();
  }

  onAddTriangle() {
    if (this.tourService.isActive()) {
      this.tourService.goTo('drawingGuideLine');
    }
    this.fabricService.addTriangle();
  }

  private async saveChangesConfirm() {
    const alert = await this.alertController.create({
      id: 'save-changements',
      header: this.translate.instant('TAB2.canvas.confirmation'),
      message: this.translate.instant('TAB2.canvas.saveChanges'),
      buttons: [
        {
          text: this.translate.instant('TAB2.canvas.no'),
          role: 'no',
          cssClass: 'secondary',
          id: 'cancel-button'
        },
        {
          text: this.translate.instant('TAB2.canvas.yes'),
          role: 'yes',
          id: 'confirm-button'
        }
      ]
    });
    return this.alertController.getTop().then(async res => {
      if (!res || (res && res.id !== 'save-changements')) {
        await alert.present();
        const { role } = await alert.onDidDismiss();
        return role;
      }
    });
  }

  onAddText() {
    this.fabricService.addText();
  }

  ngOnDestroy() {
    this.gardenSubscription.unsubscribe();
  }
}

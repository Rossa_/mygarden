/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { Tab2PageRoutingModule } from './tab2-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { Tab2Component } from './tab2.component';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';

import { GardenCanvasComponent } from './garden-canvas/garden-canvas.component';
import { GardenEditComponent } from './garden-edit/garden-edit.component';
import { HeaderComponent } from './header/header.component';
import { LoadingComponent } from './loading/loading.component';

@NgModule({
  imports: [
    IonicModule,
    FormsModule,
    FormsModule,
    CommonModule,
    TranslateModule,
    ReactiveFormsModule,
    Tab2PageRoutingModule
  ],
  declarations: [
    Tab2Component,
    HeaderComponent,
    LoadingComponent,
    GardenEditComponent,
    GardenCanvasComponent
  ],
  providers: [ScreenOrientation],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Tab2PageModule {}

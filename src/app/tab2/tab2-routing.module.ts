/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { RouterModule, Routes } from '@angular/router';
import { Tab2Component } from './tab2.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: Tab2Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Tab2PageRoutingModule {}

/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { DepositModalComponent } from '../deposit-modal/deposit-modal.component';
import { VegetableService } from '../../services/vegetable.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonModal, ModalController } from '@ionic/angular';
import { Vegetable } from '../../shared/vegetable.model';

@Component({
  selector: 'app-vegetables-list',
  templateUrl: './vegetables-list.component.html',
  styleUrls: ['./vegetables-list.component.scss']
})
export class VegetablesListComponent implements OnInit {
  @ViewChild(IonModal) modal: IonModal;
  seasonalVegetables: Vegetable[] = [];
  vegetableView: 'ascending' | 'descending' = 'ascending';
  vegetables: Vegetable[];
  vegetablesFiltered: Vegetable[];
  isDefaultTypesChanged = false;
  typesSelected: string[][] = [
    ['fruit', 'vegetable', 'herb', 'plant'],
    ['leafy', 'tuber/root', 'bulb', 'sprout', 'stem', 'seed']
  ];
  types: string[][] = [
    ['fruit', 'vegetable', 'herb', 'plant'],
    ['leafy', 'tuber/root', 'bulb', 'sprout', 'stem', 'seed']
  ];

  constructor(
    private modalCtrl: ModalController,
    public vegetablesServices: VegetableService
  ) {}

  ngOnInit() {
    this.vegetablesServices.vegetables.subscribe((vegetables: Vegetable[]) => {
      if (vegetables === null) {
        return;
      }
      this.vegetables = vegetables;
      this.updateVegetable();
      this.seasonalVegetables =
        this.vegetablesServices.getVegetablesReadyForPlanting();
    });
  }

  onSwitch(filter: string, index: number) {
    if (this.typesSelected[index].includes(filter)) {
      if (this.typesSelected.length <= 1) {
        return;
      }
      this.typesSelected[index] = this.typesSelected[index].filter(value => {
        return value !== filter;
      });
    } else {
      this.typesSelected[index].push(filter);
    }
    this.isDefaultTypesChanged =
      this.typesSelected[0].length != this.types[0].length ||
      this.typesSelected[1].length != this.types[1].length;
    this.updateVegetable();
  }

  updateVegetable() {
    this.vegetablesFiltered = this.vegetables.filter(x => {
      if (x.type[0] === 'vegetable') {
        return (
          this.typesSelected[0].includes(x.type[0]) &&
          this.typesSelected[1].includes(x.type[1])
        );
      } else {
        return this.typesSelected[0].includes(x.type[0]);
      }
    });
    this.vegetablesFiltered = this.vegetablesServices.sort(
      this.vegetablesFiltered,
      this.vegetableView
    );
  }

  async onVegetableSelected(vegetable: Vegetable) {
    // Send data to the deposit-modal component
    const modal = await this.modalCtrl.create({
      component: DepositModalComponent,
      componentProps: {
        vegetable
      }
    });
    await modal.present();
  }

  onChangeAlphabeticOrder() {
    if (this.vegetableView === 'descending') {
      this.vegetableView = 'ascending';
    } else {
      this.vegetableView = 'descending';
    }
    this.vegetablesFiltered = this.vegetablesServices.sort(
      this.vegetablesFiltered,
      this.vegetableView
    );
  }
}

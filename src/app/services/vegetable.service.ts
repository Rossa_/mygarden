/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import * as vegetableFR from '../../assets/vegetables/data-fr.json';
import * as vegetableEN from '../../assets/vegetables/data.json';
import { TranslateService } from '@ngx-translate/core';
import { Vegetable } from '../shared/vegetable.model';
import { LanguageService } from './language.service';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class VegetableService {
  vegetables = new BehaviorSubject<Vegetable[]>(null);

  constructor(
    private languageService: LanguageService,
    private translate: TranslateService
  ) {
    this.translate.onLangChange.subscribe(() => {
      this.loadVegetable();
    });
  }

  /**
   * Load vegetable form JSON
   */
  loadVegetable() {
    let data = vegetableEN;
    switch (this.languageService.getLanguage()) {
      case 'en':
        data = vegetableEN;
        break;
      case 'fr':
        data = vegetableFR;
        break;
    }
    this.vegetables.next(JSON.parse(JSON.stringify(data)).default);
  }

  /**
   * Sort the vegetables by alphabetical order.
   * @param vegetables Vegetables array to sort
   * @param order The order (ascending or descending)
   */
  sort(vegetables: Vegetable[], order: string): Vegetable[] {
    function compare(a, b) {
      if (a.name < b.name) {
        return order === 'ascending' ? -1 : 1;
      }
      if (a.name > b.name) {
        return order === 'ascending' ? 1 : -1;
      }
      return 0;
    }

    return vegetables.sort(compare);
  }

  /**
   * Outputs vegetable wanted.
   * @params {string} name
   */
  getVegetableByName(name: string): Vegetable {
    const vegetable = this.vegetables.getValue().find(s => {
      return s.name === name;
    });
    return vegetable != null ? vegetable : null;
  }

  /**
   * Outputs vegetable wanted.
   * @params {string} id
   */
  getVegetableById(id: string): Vegetable {
    const vegetable = this.vegetables.getValue().find(s => {
      return s.id === id;
    });
    return vegetable != null ? vegetable : null;
  }

  /**
   * Get vegetables.
   * @Return {Object} - Vegetables array
   */
  getVegetables(): Vegetable[] {
    return this.vegetables.getValue();
  }

  /**
   * Convert month number to str.
   * @param monthNum the month number to convert.
   * @param mode 0: accept number between 0-11, 1: accept number between 1-12
   */
  convertMonthToString(
    monthNum: number | number[],
    mode: number = 0
  ): string[] {
    const monthStr: string[] = [];
    const length: number = Array.isArray(monthNum) ? monthNum.length : 1;
    for (let i = 0; i < length; i++) {
      let d: number;
      if (Array.isArray(monthNum)) {
        d = mode === 0 ? monthNum[i] - 1 : monthNum[i]; // Convert 1-12 to 0-11 if mode = 0
      } else {
        d = mode === 0 ? monthNum - 1 : monthNum; // Convert 1-12 to 0-11 if mode = 0
      }
      const month = [];
      month[0] = this.translate.instant('GLOBAL.months.Jan');
      month[1] = this.translate.instant('GLOBAL.months.Feb');
      month[2] = this.translate.instant('GLOBAL.months.Mar');
      month[3] = this.translate.instant('GLOBAL.months.Apr');
      month[4] = this.translate.instant('GLOBAL.months.May');
      month[5] = this.translate.instant('GLOBAL.months.Jun');
      month[6] = this.translate.instant('GLOBAL.months.Jul');
      month[7] = this.translate.instant('GLOBAL.months.Aug');
      month[8] = this.translate.instant('GLOBAL.months.Sep');
      month[9] = this.translate.instant('GLOBAL.months.Oct');
      month[10] = this.translate.instant('GLOBAL.months.Nov');
      month[11] = this.translate.instant('GLOBAL.months.Dec');
      monthStr[i] = month[d];
    }
    return monthStr;
  }

  /**
   * Return the vegetable IDs corresponding to the month of planting
   * @param vegetables The Vegetable ID array to check
   * @param month A month between 0-11, default is current month
   */
  getVegetablesReadyForPlanting(
    vegetables: Vegetable[] = this.vegetables.getValue(),
    month: number = new Date().getMonth()
  ): Vegetable[] {
    month++;
    return vegetables.filter((vegetable: Vegetable) => {
      const vegetablePlantingMonth = vegetable.cultureSheets.monthOfPlanting;
      if (Array.isArray(vegetablePlantingMonth)) {
        if (vegetablePlantingMonth[0] < vegetablePlantingMonth[1]) {
          return (
            month >= vegetablePlantingMonth[0] &&
            month <= vegetablePlantingMonth[1]
          );
        } else {
          return (
            month + 12 >= vegetablePlantingMonth[0] &&
            month <= vegetablePlantingMonth[1]
          );
        }
      }
      return month === vegetablePlantingMonth;
    });
  }

  /**
   * Return a list of vegetable IDs which the month given in parameter
   * is included in the growing interval (From planting to harvest).
   * @param vegetablesId
   * @param month (0 - 11)
   */
  getGrowingVegetables(
    vegetablesId: string[],
    month: number = new Date().getMonth()
  ): string[] {
    month++; // 0-11 => 1-12
    return vegetablesId.filter((vegetableId: string) => {
      const vegetable = this.getVegetableById(vegetableId);
      const monthOfPlanting: number = Array.isArray(
        vegetable.cultureSheets.monthOfPlanting
      )
        ? Math.min(...vegetable.cultureSheets.monthOfPlanting)
        : vegetable.cultureSheets.monthOfPlanting;

      const monthOfHarvest: number = Array.isArray(
        vegetable.cultureSheets.monthOfHarvest
      )
        ? Math.min(...vegetable.cultureSheets.monthOfHarvest)
        : vegetable.cultureSheets.monthOfHarvest;
      if (monthOfPlanting > monthOfHarvest) {
        return monthOfPlanting <= month && monthOfHarvest + 12 >= month;
      } else {
        return monthOfPlanting <= month && monthOfHarvest >= month;
      }
    });
  }

  /**
   * Return an array of numbers representing the planting months for the given vegetables.
   *
   * @param {string[]} vegetablesId - An array of vegetable IDs.
   * @return {number[]} - An array of numbers representing the planting months (0-11) for the given vegetables.
   */
  getVegetablePlantingMonths(vegetablesId: string[]): number[] {
    return [
      ...new Set(
        vegetablesId
          .map(
            vegetableId =>
              this.getVegetableById(vegetableId).cultureSheets.monthOfPlanting
          ) // Get vegetables months.
          .map(x => (Array.isArray(x) ? Math.min(...x) - 1 : x - 1)) // Converts arrays to number (Select the min) amd convert 1-12 to 0-11
      ).values()
    ].sort((a: number, b: number) => a - b);
  }

  /**
   * Retrieves the vegetable timeline based on the planting month and vegetable IDs.
   *
   * @param {number[]} plantingMonth - An array of numbers representing the planting months.
   * @param {string[]} vegetablesId - An array of strings representing the vegetable IDs.
   * @returns {Object} - An object that maps each planting month to an array of corresponding growing vegetables.
   */
  getVegetableTimeline(
    plantingMonth: number[],
    vegetablesId: string[]
  ): { ['month']: string[] } {
    const plantingSchematics = new Object({}) as { ['month']: string[] };
    plantingMonth.sort().forEach((month: number) => {
      plantingSchematics[month] = this.getGrowingVegetables(
        vegetablesId.sort(),
        month
      );
    });
    return plantingSchematics;
  }

  /**
   * Returns the length of the vegetables array.
   *
   * @returns {number} The length of the vegetables array.
   */
  getVegetablesLen(): number {
    return this.vegetables.getValue().length;
  }
}

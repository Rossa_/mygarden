/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { environment } from '../../environments/environment';
import { VegetableService } from './vegetable.service';
import { StorageService } from './storage.service';
import { Garden } from '../shared/garden.model';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class MigrationService {
  migrationIsRunning = false;

  constructor(
    private storageService: StorageService,
    private router: Router,
    private vegetableService: VegetableService
  ) {}

  /**
   * Return true if the database is up-to-date
   */
  isUpToDate(): Promise<boolean> {
    return new Promise(resolve => {
      return this.storageService.getGardens().then((gardens: Garden[]) => {
        if (!gardens || gardens.length <= 0) {
          return resolve(false);
        }

        for (const garden of gardens) {
          if (
            !garden.extra ||
            !garden.extra.databaseSchemaVersion ||
            garden.extra.databaseSchemaVersion !==
              environment.lastDatabaseSchemaVersion
          ) {
            return resolve(true);
          }
        }
        return resolve(false);
      });
    });
  }

  /**
   * Update database to the new schema version.
   */
  updateDatabaseSchema() {
    this.router.navigate(['migration']).then(() => {
      this.migrationIsRunning = true;
    });
    this.storageService.getGardens().then((gardens: Garden[]) => {
      gardens.forEach(async (garden: Garden) => {
        const currentVersion = garden.extra?.databaseSchemaVersion || null;
        if (currentVersion === environment.lastDatabaseSchemaVersion) {
          return;
        }

        switch (currentVersion) {
          case null: // Update to V2.0
            const vegetables = garden.vegetable;
            garden.extra = {
              plantingMonth:
                this.vegetableService.getVegetablePlantingMonths(vegetables),
              databaseSchemaVersion: 2.0
            };
            for (let year in garden.maps) {
              let tmp = garden.maps[year];

              // Add vegetableId field
              for (let i = 0; i < tmp.objects.length; i++) {
                if (tmp.objects[i].objects[1]?.src) {
                  vegetables.forEach(vegetable => {
                    if (
                      this.vegetableService.getVegetableById(vegetable)
                        .imagePath === tmp.objects[i].objects[1].src
                    ) {
                      tmp.objects[i].vegetableId = vegetable;
                      return;
                    }
                  });
                }
              }
              tmp = JSON.stringify(tmp);

              garden.maps[year] = {};
              for (let month of garden.extra.plantingMonth) {
                garden.maps[year][month] = tmp;
              }
            }
            await this.storageService.updateGarden(garden.name, garden);
            break;
        }
        await this.router.navigate(['tabs', 'tab1']);
      });
    });
  }
}
